## WP-Packager Library

**WARNING: This project is currently UNSTABLE. New releases may break
compatibility**

Provides plugin information to your WordPress plugin, and attaches to it the
WordPress Update API for seamless and/or automatic updates for your plugins, as
if they were hosted on wordpress.org

Use with your own server or the sister project
[WP-Packager Server](https://gitlab.com/webnestors/wp-packager/server).


### Features

- Integrates with the WordPress Core Update API, having a native functionality.
- Displays plugin info such as the title, description, changelog, last updated
  and more.
- Connects with any server that provides a valid JSON schema.
- Support for premium (restricted) downloads with a license key, using a custom
  HTTP header when downloading the package.
- Currently only supports plugins.

### Usage

Use this as a library to your plugin:

```php
<?php
$remote_url = "https://my.domain.com/plugin/myplugin";
$slug = "myplugin";
$basename = "myplugin/myplugin.php";
$current_version = "1.0.0";
$updater = Webnestors\WPPackager\Updater::get_plugin_updater($remote_url, $slug, $basename, $current_version);
$updater->init();
```

You can attach a custom license key header to the download request:

```php
<?php

// ...

$updater
  // Placing '##WP-Packager-License##' as the license key will automatically
  // replace that value with the actual key when using the wp-packager/server
  // 'plugin/id/download' endpoint when downloading the package
  ->set_license_key('##WP-Packager-License##')
  // Default
  ->set_license_header('WP-License')
  ->init();

// ...
```

If the server is restricted behind basic HTTP auth:

```php
<?php

// ...

$allow_unsecure = false;
$updater
  ->set_basic_auth('username', 'password', $allow_unsecure)
  ->init();

// ...
```

If you don't want to reject unsafe URLs during downloads (unsecure http):

```php
<?php

// ...

$updater
  ->set_reject_unsafe(false)
  ->init();

// ...
```
