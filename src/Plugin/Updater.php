<?php

namespace Webnestors\WPPackager\Plugin;

use Webnestors\WPPackager\HttpException;

class Updater {

	/** @var UpdaterService */
	protected $updater_service;
	/** @var bool */
	protected $reject_unsafe = false;
	/** @var string */
	protected $license_header = 'WP-License';
	/** @var string */
	protected $license_key = null;

	/**
	 * @param UpdaterService $updater_service
	 */
	public function __construct($updater_service) {
		$this->updater_service = $updater_service;
	}

	public function init() {
		add_filter('all_plugins', $this->all_plugins(), 20, 1);
		add_filter('plugins_api', $this->plugin_info(), 20, 3);
		add_filter('site_transient_update_plugins', $this->update_plugin(), 20, 1);
		add_filter('upgrader_pre_download', $this->upgrader_pre_download(), 20, 2);
		return $this;
	}

	public function disable_cache() {
		$this->updater_service->set_cache_enabled(false);
		return $this;
	}

	/**
	 * @param int $expiration_in_seconds
	 * @return static
	 */
	public function set_cache_expiration($expiration_in_seconds) {
		$this->updater_service->set_cache_expiration($expiration_in_seconds);
		return $this;
	}

	/**
	 * Set if should reject unsecure urls with unsecure http connection to the
	 * remote server (default is false)
	 *
	 * @param bool $reject_unsafe
	 * @return $this
	 */
	public function set_reject_unsafe($reject_unsafe) {
		$this->reject_unsafe = $reject_unsafe;
		return $this;
	}

	/**
	 * @param string $username
	 * @param string $password
	 * @param bool $allow_unsecure
	 * @return static
	 */
	public function set_basic_auth($username, $password, $allow_unsecure = false) {
		if (strpos($this->updater_service->remote_url, 'http://') !== false && !$allow_unsecure) {
			throw new \Error('Security error: Unsecure connection is not allowed for basic authentication.');
		}

		$this->updater_service->set_basic_auth(base64_encode("$username:$password"));
		return $this;
	}

	/**
	 * A license key that should be passed to the remote server by the license header
	 * (see Plugin\Updater::set_license_header)
	 *
	 * @param string $key
	 * @return $this
	 */
	public function set_license_key($key) {
		$this->license_key = $key;
		return $this;
	}

	/**
	 * Sets the HTTP Header name that is going to be sent to the name server with
	 * the value of the license key (default "WP-License")
	 *
	 * @param string $header
	 * @return $this
	 */
	public function set_license_header($name) {
		$this->license_header = $name;
		return $this;
	}

	/**
	 * Used to display the "View details" link inside the plugins table
	 *
	 * @return callable
	 */
	protected function all_plugins() {
		return function($plugins) {
			if (isset($plugins[$this->updater_service->basename])) {
				$plugins[$this->updater_service->basename]['slug'] = $this->updater_service->slug;
			}
			return $plugins;
		};
	}

	/**
	 * @return callable
	 */
	protected function plugin_info() {
		/**
		 * @param false|object|array $result
		 * @param string $action
		 * @param object $args
		 */
		return function($result, $action, $args) {
			if ($action !== 'plugin_information' || $this->updater_service->slug !== $args->slug) {
				return $result;
			}

			try {
				$result = $this->updater_service->get_info() ?? false;
			} catch (HttpException $e) {
				// TODO: Errors on different response codes
			}

			return $result;
		};
	}

	/**
	 * @return callable
	 */
	protected function update_plugin() {
		/**
		 * @param \stdClass $updates The updates site transient
		 */
		return function($updates) {
			$basename = $this->updater_service->basename;

			try {
				$update = $this->updater_service->get_update();
				set_transient(\Webnestors\WPPackager\Updater::PREFIX . 'Update', $update);

				if ($update) {
					if (!isset($updates)) {
						$updates = new \stdClass();
					}
					$updates->response[$basename] = $update;
				} else {
					unset($updates->response[$basename]);
					$updates->no_update[$basename] = array();
				}
			} catch (HttpException $e) {
				// TODO: Errors on different response codes
			}

			return $updates;
		};
	}

	/**
	 * @return callable
	 */
	protected function upgrader_pre_download() {
		/**
		 * @param bool $reply
		 * @param string $package
		 */
		return function($reply, $package) {
			/** @var \Webnestors\WPPackager\Plugin\Update $update */
			$update = get_transient(\Webnestors\WPPackager\Updater::PREFIX . 'Update');
			if ($update && $update->package === $package) {
				add_filter('http_request_args', function($args, $url) use ($package) {
					if ($url !== $package)
						return $args;
					if ($this->license_key !== null)
						$args['headers'] = array($this->license_header => $this->license_key);
					$args['reject_unsafe_urls'] = $this->reject_unsafe;
					return $args;
				}, 10, 2);
			}

			return $reply;
		};
	}

}
