<?php

namespace Webnestors\WPPackager\Plugin;

use Webnestors\WPPackager\HttpException;
use Webnestors\WPPackager\Updater;

class StandardUpdaterService extends UpdaterService {

	/** @var int */
	protected $timeout = 10;

	/**
	 * @return array<mixed>
	 */
	private function get_remote() {
		// Get cached remote response
		$response = get_transient(Updater::PREFIX . $this->key);
		if ($response !== false && $this->is_cache_enabled) {
			return $response;
		}

		$this->headers['Accept'] = 'application/json';
		$options = array(
			'timeout' => $this->timeout,
			'headers' => $this->headers,
		);
		$response = wp_remote_get($this->remote_url, $options);

		if (is_wp_error($response)) {
			throw new \RuntimeException('WP_Error: ' . $response->get_error_message());
		}

		set_transient(Updater::PREFIX . $this->key, $response, $this->cache_expiration);

		return $response;
	}

	/**
	 * Requests the plugin metadata, given the service remote url is a JSON
	 * encoded file.
	 *
	 * @return array<mixed>
	 */
	public function get_remote_metadata() {
		$response = $this->get_remote();

		if (200 !== $response_code = wp_remote_retrieve_response_code($response)) {
			$http_exception = new HttpException($response_code);
			$http_exception->set_body(wp_remote_retrieve_body($response));
			throw $http_exception;
		}

		$remote_info = json_decode(wp_remote_retrieve_body($response));

		if (json_last_error() !== JSON_ERROR_NONE) {
			throw new \RuntimeException(sprintf(
				'Malformed body received from remote url: %s (valid json was expected)',
				$this->remote_url
			));
		}

		return $remote_info;
	}

	/**
	 * @return Info|null
	 */
	public function get_info() {
		$info = null;
		try {
			$remote_info = $this->get_remote_metadata();
			$remote_info->slug = $this->slug;
			$remote_info->basename = $this->basename;
			$info = new Info($remote_info);
		} catch (\RuntimeException $e) {
			trigger_error($e->getMessage(), E_USER_WARNING);
		}

		return $info;
	}

	/**
	 * @return Update|null
	 */
	public function get_update() {
		$remote_info = $this->get_info();

		if ($remote_info
			&& version_compare($this->version, $remote_info->version, '<')
			&& version_compare($remote_info->requires, get_bloginfo('version'), '<')
			&& version_compare($remote_info->requires_php, PHP_VERSION, '<')
		) {
			$update = new Update($remote_info);
			return $update;
		}

		return null;
	}

	/**
	 * @param string $credentials
	 * @return void
	 */
	public function set_basic_auth($credentials) {
		$this->headers['Authorization'] = 'Basic ' . $credentials;
	}

}
