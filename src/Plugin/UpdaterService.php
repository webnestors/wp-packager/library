<?php

namespace Webnestors\WPPackager\Plugin;

abstract class UpdaterService {

	/** @var string */
	public $remote_url;
	/** @var string */
	public $slug;
	/** @var string */
	public $basename;
	/** @var string */
	public $version;
	/** @var string */
	protected $key;
	/** @var bool */
	protected $is_cache_enabled;
	/** @var int */
	protected $cache_expiration;
	/** @var array<string> */
	protected $headers = array();

	/**
	 * @param string $remote_url
	 * @param string $slug
	 * @param string $basename
	 * @param string $version
	 */
	public function __construct($remote_url, $slug, $basename, $version) {
		$this->remote_url = $remote_url;
		$this->slug = $slug;
		$this->basename = $basename;
		$this->version = $version;
		$this->key = base64_encode($remote_url);
		$this->is_cache_enabled = true;
		$this->cache_expiration = HOUR_IN_SECONDS;
	}

	/**
	 * @param bool $is_enabled
	 * @return static
	 */
	public function set_cache_enabled($is_enabled) {
		$this->is_cache_enabled = $is_enabled;
		return $this;
	}

	/**
	 * @param int $expiration_in_seconds
	 * @return static
	 */
	public function set_cache_expiration($expiration_in_seconds) {
		$this->cache_expiration = $expiration_in_seconds;
		return $this;
	}

	/**
	 * @return string
	 */
	public function get_key() {
		return $this->key;
	}

	/**
	 * @return Info|null
	 */
	abstract public function get_info();

	/**
	 * @return Update|null
	 */
	abstract public function get_update();

	/**
	 * @param string $credentials
	 * @return void
	 */
	abstract public function set_basic_auth($credentials);

}
