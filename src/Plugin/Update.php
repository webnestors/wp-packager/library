<?php

namespace Webnestors\WPPackager\Plugin;

class Update {

	/** @var array<string> */
	public $icons;
	/** @var string */
	public $id;
	/** @var string */
	public $new_version;
	/** @var string */
	public $package;
	/** @var string */
	public $plugin;
	/** @var string */
	public $requires_php;
	/** @var string */
	public $slug;
	/** @var string */
	public $tested;
	/** @var string */
	public $upgrade_notice;
	/** @var string */
	public $url;

	/**
	 * @param Info $data
	 */
	public function __construct($data) {
		$this->icons = $data->icons;
		$this->id = '';
		$this->new_version = $data->version;
		$this->package = $data->download_link;
		$this->plugin = $data->basename;
		$this->requires_php = $data->requires_php;
		$this->slug = $data->slug;
		$this->tested = $data->tested;
		$this->upgrade_notice = $data->upgrade_notice;
		$this->url = $data->homepage;
	}

}
