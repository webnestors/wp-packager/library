<?php

namespace Webnestors\WPPackager\Plugin;

class Info {

	/** @var string */
	public $author;
	/** @var array<string> */
	public $banners;
	/** @var string */
	public $basename;
	/** @var string */
	public $download_link;
	/** @var string */
	public $homepage;
	/** @var array<string> */
	public $icons;
	/** @var string */
	public $last_updated;
	/** @var string */
	public $name;
	/** @var string */
	public $requires;
	/** @var string */
	public $requires_php;
	/** @var array<string> */
	public $sections;
	/** @var string */
	public $slug;
	/** @var string */
	public $tested;
	/** @var string */
	public $upgrade_notice;
	/** @var string */
	public $version;

	/**
	 * @param object $data
	 */
	public function __construct($data) {
		$this->author = $this->author($data->author, $data->author_homepage ?? null);
		$this->basename = $data->basename;
		$this->download_link = $data->download_url ?? '';
		$this->homepage = $data->homepage ?? '';
		$this->last_updated = $data->last_updated ?? '';
		$this->name = $data->name ?? 'Plugin Name Placeholder';
		$this->requires = $data->requires ?? '';
		$this->requires_php = $data->requires_php ?? '';
		$this->slug = $data->slug;
		$this->tested = $data->tested ?? '';
		$this->upgrade_notice = $data->upgrade_notice ?? null;
		$this->version = $data->version ?? '1.0.0';

		$this->sections = array(
			'description' => $data->sections->description ?? '',
			'installation' => $data->sections->installation ?? '',
			'changelog' => $data->sections->changelog ?? '',
		);

		$this->icons = array(
			'svg' => $data->icons->svg ?? '',
			'1x' => $data->icons->{'1x'} ?? '',
			'2x' => $data->icons->{'2x'} ?? '',
			'default' => $data->icons->default ?? '',
		);

		$this->banners = array(
			'low' => $data->banners->low ?? '',
			'high' => $data->banners->high ?? '',
		);
	}

	/**
	 * @param string $author
	 * @param string $author_homepage
	 * @return string
	 */
	private function author($author, $author_homepage) {
		return isset($author_homepage) ? '<a href="' . esc_url($author_homepage) . '">' . $author . '</a>' : $author;
	}

}
