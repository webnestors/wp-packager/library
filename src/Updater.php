<?php

namespace Webnestors\WPPackager;

class Updater {

	const PREFIX = 'wppackager_';

	/**
	 * @param string $remote_url
	 * @param string $slug
	 * @param string $basename
	 * @param string $version
	 * @return \Webnestors\WPPackager\Plugin\Updater
	 */
	public static function get_plugin_updater($remote_url, $slug, $basename, $version) {
		$host = parse_url($remote_url, PHP_URL_HOST);

		switch ($host) {
		case 'gitlab.com':
			// FIXME: Not implemented
		case 'github.com':
			// FIXME: Not implemented
		default:
			$updater_service = new Plugin\StandardUpdaterService($remote_url, $slug, $basename, $version);
			break;
		}

		return new Plugin\Updater($updater_service);
	}

	/**
	 * @param string $remote_url
	 * @param string $slug
	 * @param string $basename
	 * @param string $version
	 * @return void
	 */
	public static function get_theme_updater($remote_url, $slug, $basename, $version) {
		// FIXME: Not implemented
	}

}
